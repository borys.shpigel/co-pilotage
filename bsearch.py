import requests
from bs4 import BeautifulSoup

# Search query
query = 'chatgpt logo filetype:png'

# Make a GET request to the Bing search page
response = requests.get(f'https://www.bing.com/images/search?q={query}')

# Parse the HTML source code
soup = BeautifulSoup(response.text, 'html.parser')

# Extract the URLs of the first 3 images
image_urls = [img['src'] for img in soup.select('.img_cont a img')[:3]]

# Print the URLs
for url in image_urls:
    print(url)
