from flask import Flask, render_template
from datetime import datetime, timedelta
import getpass


app = Flask(__name__)

@app.route('/')
def index():
    date = datetime.now().date().strftime('%Y-%m-%d')
    time = datetime.now().time().strftime('%H:%M:%S')
    start_date = datetime(2022,2,24)
    days_war = (datetime.now() - start_date).days
    username = getpass.getuser()
    logo_url = "https://ds.static.rtbf.be/article/image/1920xAuto/0/f/c/5e6bd7a6970cd4325e587f02667f7f73-1564569809.png"
    return render_template('index.html', name=username, date=date, time=time, days_war=days_war, logo_url=logo_url)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)