from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup

# Search query
query = 'chatgpt logo filetype:png'

# Start the web driver
driver = webdriver.Firefox()

# Navigate to the Bing search page
driver.get("https://www.bing.com/images/")

# Find the search box element and enter the query
search_box = driver.find_element_by_name("q")
search_box.send_keys(query)
search_box.send_keys(Keys.RETURN)

# Wait for the page to load
driver.implicitly_wait(10)

# Get the page source
html = driver.page_source

# Close the driver
driver.quit()

# Parse the HTML source code
soup = BeautifulSoup(html, 'html.parser')

# Extract the URLs of the images
image_urls = [img['src'] for img in soup.select('.img_cont a img')]

# Print the URLs
for url in image_urls:
    print(url)
