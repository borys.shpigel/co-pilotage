FROM python:3.8-alpine

COPY . /app

WORKDIR /app

RUN python3 -m pip install --upgrade pip

RUN pip install -r requirements.txt

EXPOSE 5000

CMD ["python", "app.py"]